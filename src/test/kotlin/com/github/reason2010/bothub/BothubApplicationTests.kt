package com.github.reason2010.bothub

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class BothubApplicationTests {

	@Test
	fun contextLoads() {
	}

}
