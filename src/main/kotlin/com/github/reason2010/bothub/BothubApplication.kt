package com.github.reason2010.bothub

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
open class BothubApplication {
    @Bean
    open fun init(repository: UserRepository): CommandLineRunner {
        return CommandLineRunner {
            repository.save(User("Jack", "Bauer"))
        }
    }
}

@Document
data class User (
    var firstName: String = "",
    var lastName: String = ""
)

interface UserRepository : CrudRepository<User, String> {
    fun findByLastName(name: String): List<User>
}

@RestController
class UserController @Autowired constructor(val repository: UserRepository) {
    @RequestMapping("/")
    fun findAll() = repository.findAll()
    @RequestMapping("/{name}")
    fun findByLastName(@PathVariable name: String) = repository.findByLastName(name)
}

fun main(args: Array<String>) {
    SpringApplication.run(BothubApplication::class.java, *args)
}
